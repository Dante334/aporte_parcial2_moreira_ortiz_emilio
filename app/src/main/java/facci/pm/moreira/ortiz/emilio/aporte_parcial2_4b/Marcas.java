package facci.pm.moreira.ortiz.emilio.aporte_parcial2_4b;

import com.orm.SugarRecord;

public class Marcas extends SugarRecord <Marcas> {
     String IMEI, gama, marca, modelo, compañia_telefonica;

    public Marcas(String IMEI, String gama, String marca, String modelo, String compañia_telefonica) {
        this.IMEI = IMEI;
        this.gama = gama;
        this.marca = marca;
        this.modelo = modelo;
        this.compañia_telefonica = compañia_telefonica;
    }

    public Marcas (){

    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getGama() {
        return gama;
    }

    public void setGama(String gama) {
        this.gama = gama;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCompañia_telefonica() {
        return compañia_telefonica;
    }

    public void setCompañia_telefonica(String compañia_telefonica) {
        this.compañia_telefonica = compañia_telefonica;
    }
}
