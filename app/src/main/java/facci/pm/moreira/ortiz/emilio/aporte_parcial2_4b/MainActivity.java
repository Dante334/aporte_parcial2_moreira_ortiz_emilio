package facci.pm.moreira.ortiz.emilio.aporte_parcial2_4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Marcas marcas = new Marcas ("IMEI", "gama", "marca", "modelo","compañia_telefonica");
        marcas.save();
        informacion();
        Actualizar();
        EliminarDato();
    }

    public void informacion(){
        Marcas info = Marcas.findById(Marcas.class, Long.parseLong("1"));
        Log.i("Marcas",info.getMarca());
        Log.i("IMEI",info.getIMEI());
        Log.i("gama",info.getGama());
        Log.i("modelo",info.getModelo());
        Log.i("compañia_telefonica",info.getCompañia_telefonica());
    }

    public void Actualizar(){
        Marcas actu = Marcas.findById(Marcas.class, Long.parseLong("1"));
        actu.marca = "Claro";
        actu.compañia_telefonica = "tuenti";
        actu.gama ="gama2";
        actu.IMEI = "falk3";
        actu.modelo = "samsung";
        actu.save();
        Log.i("titulo","informacion actualizado");
        informacion();
    }

    public void EliminarDato(){
        Marcas delete = Marcas.findById(Marcas.class, Long.parseLong("1"));
        delete.delete();
        Log.i("eliminacion","datos eliminados");
    }

}
